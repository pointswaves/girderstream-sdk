# A basic sdk for Girderstream projects

This project provides a freedesktop-sdk based sdk for girderstream.

Currently the project just imports the binaries created by bst into girderstream.

These can then be used through a junction to build girderstream project.

However [grdtools](https://gitlab.com/pointswaves/grdtools/) now exists and builds
all elements its self and so should be used instead of this project unless you
have a very spesifi need for freedesktop element.

This repo is no longer being actively developed by the auther, please use grdtools
unless you want to maintain this repo your self.